#include "alzai.h"
#include "features/key_overrides.c"
#include "features/oneshot.h"
#include "keymap_canadian_multilingual.h"
// clang-format off
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_CDH] = LAYOUT_alzai(
    //  .--------+--------+--------+--------+--------.   .--------+--------+--------+--------+--------.
         CA_Q    ,CA_W    ,CA_F    ,CA_P    ,CA_B    ,    CA_J    ,CA_L    ,CA_U    ,CA_Y    ,KC_QUOT ,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
         CA_A    ,CA_R    ,CA_S    ,CA_T    ,CA_G    ,    CA_M    ,CA_N    ,CA_E    ,CA_I    ,CA_O    ,
    //  |--------+--------+--------+--------+--------|   |--------+--------|--------+--------+--------|
         CA_Z    ,CA_X    ,CA_C    ,CA_D    ,CA_V    ,    CA_K    ,CA_H    ,KC_COMMA,KC_DOT  ,KC_SLSH ,
    //  '--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------'
                                    NAV     ,KC_SPACE,    OSM_LSFT,NUM
    //                             '--------+--------'   '--------+--------'
    ),
    [_NAV] = LAYOUT_alzai(
    //  .--------+--------+--------+--------+--------.   .--------+--------+--------+--------+--------.
         KC_ESC  ,BACK    ,FWD     ,SW_WIN  ,SCRNSHT ,    __NONE__,KC_STAB ,KC_UP   ,KC_TAB  ,__NONE__,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
         OS_CTRL ,OS_ALT  ,OS_SHFT ,OS_CMD  ,SW_LANG  ,    GAME   ,KC_LEFT ,KC_DOWN ,KC_RGHT ,KC_ENTER,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
         KC_F5    ,KC_F10 ,KC_F11  ,KC_F12  ,LOCK    ,    CA_CCED ,CA_EACU ,CA_EGRV ,CA_AGRV , CA_UGRV,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
                                    ________,__NONE__,    KC_BSPC ,________
    //                             '--------+--------'   '--------+--------'
    ),
    [_NUM] = LAYOUT_alzai(
    //  .--------+--------+--------+--------+--------.   .--------+--------+--------+--------+--------.
         KC_NLCK ,KC_KP_7 ,KC_KP_8 ,KC_KP_9 ,KC_VOLU ,    KC_MNXT ,M_L2_3RD,M_LT_HF ,M_RT_HF ,M_R2_3RD,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
         __NONE__,KC_KP_4 ,KC_KP_5 ,KC_KP_6 ,KC_VOLD ,    KC_MPLY ,OS_CMD  ,OS_SHFT ,OS_ALT  ,OS_CTRL ,
    //  |--------+--------+--------+--------+--------|   |--------+--------|--------+--------+--------|
         __NONE__,KC_KP_1 ,KC_KP_2 ,KC_KP_3 ,KC_MUTE ,    KC_MPRV ,M_LT_3RD,M_MD_3RD,M_RT_3RD,M_FSCRN ,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------'
                                    ________,KC_KP_0 ,    __NONE__,________
    //                             '--------+--------'   '--------+--------'
    ),
    [_SYM] = LAYOUT_alzai(
    //  .--------+--------+--------+--------+--------.   .--------+--------+--------+--------+--------.
         CA_PERC ,CA_CIRC ,CA_DLR  ,CA_EURO ,__NONE__,    CA_QUES ,CA_LCBR ,CA_RCBR ,CA_AT   ,CA_HASH ,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
         CA_SLSH ,CA_ASTR ,CA_MINS ,CA_PLUS ,CA_BSLS ,    __NONE__,CA_LPRN ,CA_RPRN ,KC_LT   ,KC_GT   ,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
         CA_PIPE ,CA_AMPR ,CA_EXLM ,CA_EQL  ,CA_LABK ,    CA_RABK ,CA_LBRC ,CA_RBRC ,CA_TILD ,CA_GRV  ,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
                                    ________,__NONE__,    __NONE__,________
    //                             '--------+--------'   '--------+--------'
    ),
    [_GAME_0] = LAYOUT_alzai(
    //  .--------+--------+--------+--------+--------.   .--------+--------+--------+--------+--------.
         KC_1    ,KC_2    ,KC_3    ,KC_4    ,KC_5    ,    __NONE__,__NONE__,__NONE__,__NONE__,__NONE__,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
         KC_LCTL ,KC_Q    ,KC_W    ,KC_E    ,KC_R    ,    BASE    ,__NONE__,__NONE__,__NONE__,__NONE__,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
         KC_LSFT ,KC_A    ,KC_S    ,KC_D    ,KC_F    ,    __NONE__,__NONE__,__NONE__,__NONE__,__NONE__,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
                                    ESC_G2  ,SPC_G1  ,    __NONE__,__NONE__
    //                             '--------+--------'   '--------+--------'
    ),
    [_GAME_1] = LAYOUT_alzai(
    //  .--------+--------+--------+--------+--------.   .--------+--------+--------+--------+--------.
         S(KC_1) ,S(KC_2) ,S(KC_3) ,S(KC_4) ,S(KC_5) ,    __NONE__,__NONE__,__NONE__,__NONE__,__NONE__,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
         KC_N    ,KC_Q    ,KC_W    ,S(KC_E) ,S(KC_R) ,    __NONE__,__NONE__,__NONE__,__NONE__,__NONE__,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
         KC_P    ,KC_B    ,KC_C    ,KC_M    ,S(KC_F) ,    __NONE__,__NONE__,__NONE__,__NONE__,__NONE__,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
                                    __NONE__,________,    __NONE__,__NONE__
    //                             '--------+--------'   '--------+--------'
    ),
    [_GAME_2] = LAYOUT_alzai(
    //  .--------+--------+--------+--------+--------.   .--------+--------+--------+--------+--------.
         C(KC_1) ,C(KC_2) ,C(KC_3) ,C(KC_4) ,C(KC_5) ,    __NONE__,__NONE__,__NONE__,__NONE__,__NONE__,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
         __NONE__,KC_Q    ,KC_W    ,C(KC_E) ,C(KC_R) ,    __NONE__,__NONE__,__NONE__,__NONE__,__NONE__,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
         __NONE__,__NONE__,__NONE__,__NONE__,C(KC_F) ,    __NONE__,__NONE__,__NONE__,__NONE__,__NONE__,
    //  |--------+--------+--------+--------+--------|   |--------+--------+--------+--------+--------|
                                    ________,KC_LSFT ,    __NONE__,__NONE__
    //                             '--------+--------'   '--------+--------'
    )
};
// clang-format on

#if defined(COMBO_ENABLE)
#include "g/keymap_combo.h" // to make combo def dictionary work
#endif

layer_state_t layer_state_set_user(layer_state_t state) {
  return update_tri_layer_state(state, _NAV, _NUM, _SYM);
};

bool is_oneshot_cancel_key(uint16_t keycode) {
  switch (keycode) {
  case NUM:
  case NAV:
    return true;
  default:
    return false;
  }
}

bool is_oneshot_ignored_key(uint16_t keycode) {
  switch (keycode) {
  case NUM:
  case NAV:
  case OSM_LSFT:
  case OS_SHFT:
  case OS_CTRL:
  case OS_ALT:
  case OS_CMD:
    return true;
  default:
    return false;
  }
}

bool sw_win_active = false;
bool sw_lang_active = false;

oneshot_state os_shft_state = os_up_unqueued;
oneshot_state os_ctrl_state = os_up_unqueued;
oneshot_state os_alt_state = os_up_unqueued;
oneshot_state os_cmd_state = os_up_unqueued;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  update_swapper(&sw_win_active, KC_LGUI, KC_TAB, SW_WIN, keycode, record);

  update_oneshot(&os_shft_state, KC_LSFT, OS_SHFT, keycode, record);
  update_oneshot(&os_ctrl_state, KC_LCTL, OS_CTRL, keycode, record);
  update_oneshot(&os_alt_state, KC_LALT, OS_ALT, keycode, record);
  update_oneshot(&os_cmd_state, KC_LCMD, OS_CMD, keycode, record);

  return true;
}
