#!/bin/bash

cwd=$(pwd)

ln -sf "$cwd/users/alzai/" "$cwd/../qmk_firmware/users/"
ln -sf "$cwd/keyboards/ferris/keymaps/alzai/" "$cwd/../qmk_firmware/keyboards/ferris/keymaps/"
